(ns pokeapi.system-tests
  (:require
   [clojure.test :refer [deftest is use-fixtures]]
   [pokeapi.schema :refer [load-schema]]
   [pokeapi.test-utils :refer [simplify]]
   [pokeapi.server :refer [app-handler]]
   [ring.adapter.jetty :as jetty]
   [com.walmartlabs.lacinia :as lacinia]))

(defn ^:private test-server
  []
  (let [server (jetty/run-jetty #'app-handler {:port 8989 :join? false})]
    server))

(def ^:dynamic ^:private *test-server*)

(use-fixtures :once
  (fn [test-fn]
    (binding [*test-server* (test-server)]
      (try
        (test-fn)
        (finally
          (.stop *test-server*))))))

(defn ^:private q
  [query-string]
  (-> (lacinia/execute (load-schema) query-string nil nil)
      simplify))

(deftest can-query-ivysaur
  (let [results (q "{ pokemon_by_id (id:2) { id name height weight is_default }}")]
    (is (= {:data {:pokemon_by_id {:id 2, :name "ivysaur", :height 10, :weight 130, :is_default true}}}
           results))))

(deftest can-query-ivysaur-stats
  (is (= (q "{ pokemon_by_id (id:2) { stats { id name }}}")
         {:data {:pokemon_by_id {:stats [{:id 1, :name "hp"}
                                         {:id 2, :name "attack"}
                                         {:id 3, :name "defense"}
                                         {:id 4, :name "special-attack"}
                                         {:id 5, :name "special-defense"}
                                         {:id 6, :name "speed"}]}}})))

(deftest can-query-ivysaur-types
  (is (= (q "{ pokemon_by_id (id:2) { types { id name }}}")
         {:data {:pokemon_by_id {:types [{:id 12, :name "grass"} {:id 4, :name "poison"}]}}})))

(deftest can-query-ivysaur-species
  (is (= (q "{ pokemon_by_id (id:2) { species { id egg_groups flavor_text genus }}}")
         {:data {:pokemon_by_id {:species {:id 2, :egg_groups ["plant" "monster"], :flavor_text "There is a bud on this Pokémon’s back. To support its weight, Ivysaur’s legs and trunk grow thick and strong. If it starts spending more time lying in the sunlight, it’s a sign that the bud will bloom into a large flower soon.", :genus "Seed Pokémon"}}}})))

(deftest can-query-ivysaur-abilities
  (is (= (q "{ pokemon_by_id (id:2) { abilities { id name }}}")
         {:data {:pokemon_by_id {:abilities [{:id 34, :name "chlorophyll"} {:id 65, :name "overgrow"}]}}})))

(deftest can-list-pokemon
  (let [pokemons (get-in (q "{ list_pokemon { id }}") [:data :list_pokemon])]
    (is (= (count pokemons)) 964)))

(deftest can-list_pokemon_with_types
  (is (= (q "{ list_pokemon_with_types(type_names: [\"poison\", \"grass\"]) { name }}")
         {:data {:list_pokemon_with_types [{:name "bulbasaur"} {:name "ivysaur"} {:name "venusaur"}
                                           {:name "oddish"} {:name "gloom"} {:name "vileplume"}
                                           {:name "bellsprout"} {:name "weepinbell"} {:name "victreebel"}
                                           {:name "roselia"} {:name "budew"} {:name "roserade"}
                                           {:name "foongus"} {:name "amoonguss"} {:name "venusaur-mega"}]}})))

(deftest can-list_pokemon_with_empty_types
  (is (= (q "{ list_pokemon_with_types(type_names: []) { name }}")
         {:data {:list_pokemon_with_types []}})))
