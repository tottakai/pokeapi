FROM clojure:lein-2.8.3-alpine
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY project.clj /usr/src/app
RUN lein deps
COPY . /usr/src/app
RUN mv "$(lein uberjar | sed -n 's/^Created \(.*standalone\.jar\)/\1/p')" pokeapi-app-standalone.jar
CMD ["java", "-jar", "pokeapi-app-standalone.jar"]
EXPOSE 5000/tcp
