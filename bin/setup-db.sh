#!/usr/bin/env bash

docker exec -i --user postgres pokeapi_db createdb pokedb

docker exec -i --user postgres pokeapi_db psql pokedb -a  <<__END
create user poke_role with superuser password 'password';
__END