docker network create -d bridge pokebridge
docker run --rm --network=pokebridge -d -p 25433:5432 -e POSTGRES_PASSWORD=password --name=pokepostgres postgres:10.6-alpine
docker exec -i --user postgres pokepostgres createdb pokedb
docker run --rm --network=pokebridge -e DATABASE_HOST=pokepostgres -e DATABASE_PORT=5432 -e DATABASE_USERNAME=postgres -e DATABASE_PASSWORD=password -it tottakai/pokeapi lein migrate
docker run --rm --network=pokebridge -p 12345:5000 -e DATABASE_HOST=pokepostgres -e DATABASE_PORT=5432 -e DATABASE_USERNAME=postgres -e DATABASE_PASSWORD=password -it tottakai/pokeapi