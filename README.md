# pokeapi

GraphQL Api for Pokémon.

## Run development

    Run server interactively ´lein ring server 8888´. Will refresh when files change.

## Run tests

    Continously run tests with ´lein test-refresh´.

## Deployment

    Deployment is handled automatically by Azure DevOps pipelines.
    
    Deploy to Azure: ```docker build -t tottakai/pokeapi:latest . && docker push tottakai/pokeapi:latest```

## License

Copyright © 2019 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
