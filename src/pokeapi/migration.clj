(ns pokeapi.migration
  (:require
   [pokeapi.db :as db]
   [ragtime.jdbc :as jdbc]
   [ragtime.repl :as repl]))

(defn load-config []
  {:datastore (jdbc/sql-database db/db-spec)
   :migrations (jdbc/load-resources "migrations")})

(defn migrate []
  (repl/migrate (load-config)))

(defn rollback []
  (repl/rollback (load-config)))