(ns pokeapi.auth
  (:require
   [pokeapi.db :as db]
   [pokeapi.config :as config]
   [buddy.auth.backends :as auth.backends]
   [buddy.auth.middleware :as auth.middleware]
   [buddy.hashers :as hs]
   [buddy.sign.jwt :as jwt]))

(defn ^:private create-auth-token
  [username]
  (jwt/sign {:username username} config/auth-secret))

(defn ^:private auth-token
  [db token]
  (try
    (let [claim (jwt/unsign token config/auth-secret)]
      (db/find-user db (:username claim)))
    (catch Exception _e false)))

(defn ^:private authfn
  [_request token]
  (auth-token db/pokemon-db token))

(def ^:private token-auth-backend
  (auth.backends/token {:authfn authfn}))

(def wrap-token-authentication
  #(auth.middleware/wrap-authentication % token-auth-backend))

(defn login-user
  [db username password]
  (let [user (db/find-user db username)]
    (if (and user (hs/check password (:password user)))
      {:username username :token (create-auth-token username) :role (:role user)}
      (throw (Exception. "Invalid username or password.")))))

(defn register-user!
  [db username password]
  (let [hash (hs/encrypt password)]
    (if (db/add-user! db username hash)
      (create-auth-token username)
      (throw (Exception. "invalid login.")))))

