(ns pokeapi.config
  (:require
   [environ.core :refer [env]]))

(def auth-secret (or (env :secret-key) "kissatekeepesääpuuhun"))

(def database-config
  {:host (or (env :database-host) "localhost")
   :port (or (env :database-port) 25433)
   :user (or (env :database-username) "postgres")
   :password (or (env :database-password) "password")})

(def server-port (or (env :port) 5000))