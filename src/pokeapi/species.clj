(ns pokeapi.species
  (:require
   [clojure.string :as string]
   [clojure.tools.logging :as log]
   [clj-http.client :as client]
   [cheshire.core :as json]))

(def english?
  #(= (get-in % [:language :name])
      "en"))

(defn flavor-text
  [species]
  (->> species
       :flavor_text_entries
       (filter english?)
       first
       :flavor_text))

(defn genus
  [species]
  (->> species
       :genera
       (filter english?)
       first
       :genus))

(defn egg-groups
  [species]
  (map :name (:egg_groups species)))

(defn transform-species
  [species]
  (let [id (:id species)
        flavor-text (string/replace (flavor-text species) "\n" " ")
        genus (genus species)
        egg-groups (egg-groups species)]
    {:id id
     :flavor_text flavor-text
     :genus genus
     :egg_groups egg-groups}))

(defn get-species-by-id
  [pokemon-id]
  (try
    (->
     (client/get (format "https://pokeapi.co/api/v2/pokemon-species/%s/" pokemon-id))
     (:body)
     (json/decode true)
     transform-species)
    (catch Exception _e
      (log/debug (format "Error occurred when retrieving pokemon %s species" pokemon-id))
      nil)))
