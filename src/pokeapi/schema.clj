(ns pokeapi.schema
  "Contains custom resolves and a function to provide the full schema"
  (:require [clojure.java.io :as io]
            [clojure.edn :as edn]
            [clojure.tools.logging :as log]
            [com.walmartlabs.lacinia.util :as util]
            [com.walmartlabs.lacinia.schema :as schema]
            [com.walmartlabs.lacinia.resolve :as resolve]
            [com.walmartlabs.lacinia.executor :as executor]
            [pokeapi.db :as db]
            [pokeapi.species :as s]
            [pokeapi.auth :as auth]))

(defn auth-for-role?
  [context role]
  (if-let [identity (:identity context)]
    (and identity (= (identity :role) role))
    false))

(defn pokemon-by-id
  [db]
  (fn [_ args _]
    (if-let [pokemon (db/find-pokemon-by-id db (:id args))]
      pokemon
      (resolve/with-error nil {:message (format "Pokemon with id %s doesn't exist." (:id args))}))))

(comment
  (user/q "{ pokemon_by_id(id: 3) { id name }}")
  (user/net-query "{ pokemon_by_id(id: 3) { id name }}"
                  "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6InRvbWlAdG90dGFrYWkubmV0In0.BpvnbsmepDEpTSP7Y5AVoIcdz_SR3vzvEspzy9FSLLc")
  ...)

(defn pokemon-sprite-front-default
  []
  (fn [_ _ pokemon]
    (format "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/%s.png", (:id pokemon))))

(defn pokemon-species
  []
  (fn [_ _ pokemon]
    (s/get-species-by-id (:id pokemon))))

(defn pokemon-stats
  [db]
  (fn [context _ pokemon]
    (let [pokemon-id (:id pokemon)
          stats (or (:pokeapi.schema/stats context) (db/list-stats-for-pokemon db [pokemon-id]))]
      (get stats pokemon-id))))

(comment
  (user/q "{list_pokemon {id name abilities { name id }}}")
  (user/q "{pokemon_by_id(id:3) {name stats {name id}}}")
 ;(user/q "{list_pokemon {id name }}")
  ...)

(defn pokemon-types
  [db]
  (fn [context _ pokemon]
    (let [pokemon-id (:id pokemon)
          types (or (:pokeapi.schema/types context) (db/list-types-for-pokemon db [pokemon-id]))]
      (get types pokemon-id))))

(defn pokemon-abilities
  [db]
  (fn [context _ pokemon]
    (let [pokemon-id  (:id pokemon)
          abilities (or (:pokeapi.schema/abilities context) (db/list-abilities-for-pokemon db [pokemon-id]))]
      (get abilities pokemon-id))))

(defn create-subquery-includer
  [context db pokemons]
  (fn
    [field-name query-fn]
    (if (executor/selects-field? context field-name)
      (query-fn db (map :id pokemons)))))

(defn list-pokemons
  [db]
  (fn [context _ _]
    (let [pokemons (db/list-pokemons db)
          include? (create-subquery-includer context db pokemons)
          stats (include? :Pokemon/stats db/list-stats-for-pokemon)
          types (include? :Pokemon/types db/list-types-for-pokemon)
          abilities (include? :Pokemon/abilities db/list-abilities-for-pokemon)]
      (resolve/with-context pokemons {::stats stats
                                      ::types types
                                      ::abilities abilities}))))

(defn list-pokemon-with-types
  [db]
  (fn [_ args _]
    (db/list-pokemon-with-types db (:type_names args))))

(defn login-user
  [db]
  (fn [_ args _]
    (let [{:keys [email password]} args]
      (try
        (:token (auth/login-user db email password))
        (catch Exception e
          (log/info (str "user with email: " email " tried to login with invalid details"))
          (resolve/with-error nil {:message (.getMessage e)}))))))

(defn register-user!
  [db]
  (fn [_ args _]
    (let [{:keys [email password]} args]
      (try
        (auth/register-user! db email password)
        (catch Exception e
          (log/info (format "Invalid registering attempt by user: %s", email))
          (resolve/with-error nil {:message (.getMessage e)}))))))

(defn promote-user-to-admin-role!
  [db]
  (fn [context args _]
    (log/info "IDENTITY:" (:identity context))
    (if (auth-for-role? context "admin")
      (db/promote-user-to-admin-role! db (:email args))
      (resolve/with-error nil {:message "I sufficient rights to promote user to admin role."}))))

(defn resolve-map
  []
  (let [db db/pokemon-db]
    {:query/pokemon-by-id (pokemon-by-id db)
     :Pokemon/sprite-front-default (pokemon-sprite-front-default)
     :Pokemon/species (pokemon-species)
     :Pokemon/stats (pokemon-stats db)
     :Pokemon/types (pokemon-types db)
     :Pokemon/abilities (pokemon-abilities db)
     :query/list-pokemons (list-pokemons db)
     :query/list-pokemon-with-types (list-pokemon-with-types db)
     :query/login-user (login-user db)
     :mutation/register-user (register-user! db)
     :mutation/promote-user-to-admin-role (promote-user-to-admin-role! db)}))

(defn load-schema
  []
  (log/debug "load schema")
  (-> (io/resource "pokemon-schema.edn")
      slurp
      edn/read-string
      (util/attach-resolvers (resolve-map))
      schema/compile))