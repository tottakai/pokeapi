(ns pokeapi.server
  (:gen-class)
  (:require
   [clojure.data.json :as json]
   [clojure.tools.logging :as log]
   [pokeapi.schema :as s]
   [pokeapi.auth :refer [wrap-token-authentication]]
   [pokeapi.utils :as u]
   [pokeapi.config :as config]
   [com.walmartlabs.lacinia :as lacinia]
   [compojure.core :refer [defroutes, GET, POST, ANY]]
   [ring.adapter.jetty :as jetty]
   [ring.util.request :refer [body-string]]
   [ring.util.response :refer [response redirect]]
   [ring.middleware.cors :refer [wrap-cors]]
   [ring.middleware.params :refer [wrap-params]]
   [ring.middleware.resource :refer [wrap-resource]]
   [ring.middleware.content-type :refer [wrap-content-type]]))

(defn ^:private index-handler
  [_request]
  (redirect "/index.html"))

(defn ^:private ping
  [_request]
  (response "hello world!!"))

(defn ^:private admin
  [request]
  (let [{:keys [name role]} (:identity request)]
    (log/info (format "user %s authenticating" name))
    (if (= role "admin")
      (response "Success! You have role 'admin'")
      {:status 403})))

(defn ^:private graphql-handler
  [compiled-schema]
  (let [context {:cache (atom {})}]
    (fn [request]
      (let [identity (:identity request)
            vars (u/variable-map request)
            query (u/extract-query request)
            result (lacinia/execute compiled-schema query vars (assoc context :identity identity))
            status (if (-> result :errors seq)
                     400
                     200)]
        {:status status
         :headers {"Content-Type" "application/json"}
         :body (json/write-str result)}))))

(defn ^:private not-found-handler
  [_request]
  {:status 404
   :headers {"Content-Type" "text/html"}
   :body "Only GraphQL JSON requests to /graphql are accepted on this server"})

(def pokeapi-schema (s/load-schema))

(defroutes app-routes
  (GET "/" [] index-handler)
  (GET "/graphiql" [] index-handler)
  (GET "/ping" [] ping)
  (GET "/admin" [] admin)
  (POST "/graphql" [] (graphql-handler pokeapi-schema))
  (ANY "*" [] not-found-handler))

(defn wrap-body-string [handler]
  (fn [request]
    (let [body-str (body-string request)]
      (handler (assoc request :body body-str)))))

(def app-handler
  (-> app-routes
      wrap-params
      (wrap-cors :access-control-allow-origin [#".*"]
                 :access-control-allow-methods [:get :post])
      (wrap-token-authentication)
      wrap-body-string
      (wrap-resource "graphiql")
      (wrap-resource "/")
      wrap-content-type))

(defn -main [& [port]]
  (let [port (Integer. (or port config/server-port))]
    (log/info "Started on port:" port)
    (jetty/run-jetty #'app-handler {:port port :join? false})))
