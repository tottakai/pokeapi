(ns pokeapi.db
  (:require
   [jdbc.core :as jdbc]
   [clojure.tools.logging :as log]
   [clojure.string :refer [join]]
   [pokeapi.config :as config])
  (:import (com.mchange.v2.c3p0 ComboPooledDataSource)))

(def db-spec (merge
              {:classname "org.postgresql.Driver"
               :dbtype "postgresql"
               :dbname "pokedb"}
              config/database-config))

(log/debug (str "postgres " (db-spec :host) ":" (db-spec :port)))

(defn ^:private pooled-data-source
  [db-spec]
  (let [{:keys [classname dbtype host port dbname user password]} db-spec]
    {:datasource
     (doto (ComboPooledDataSource.)
       (.setDriverClass classname)
       (.setJdbcUrl (str "jdbc:" dbtype "://" host ":" port "/" dbname))
       (.setUser user)
       (.setPassword password))}))

(defn ^:private query
  [db statement]
  (log/debug "QUERY:" statement)
  (with-open [conn (jdbc/connection db)]
    (jdbc/fetch conn statement)))

(def pokemon-db
  (pooled-data-source db-spec))

(defn find-pokemon-by-id
  [db pokemon-id]
  (->
   (query db
          ["select pokemon_id as id, name, height, weight, is_default from pokemon where pokemon_id = ?", pokemon-id])
   first))

(defn list-stats-for-pokemon
  [db pokemon-ids]
  (let [ids (join ", " pokemon-ids)]
    (->>
     (query db
            [(format "select pokemon_stats.pokemon_id, stats.stat_id as id, stats.name, pokemon_stats.base_stat, stats.is_battle_only 
                         from stats 
                         inner join pokemon_stats on pokemon_stats.stat_id = stats.stat_id 
                         where pokemon_stats.pokemon_id in (%s)" ids)])
     (group-by :pokemon_id))))

(defn list-types-for-pokemon
  [db pokemon-ids]
  (let [ids (join ", " pokemon-ids)]
    (->>
     (query db
            [(format "select pokemon_types.pokemon_id, types.type_id as id, types.name 
                         from types 
                         inner join pokemon_types on pokemon_types.type_id = types.type_id 
                         where pokemon_types.pokemon_id in (%s)" ids)])
     (group-by :pokemon_id))))

(defn list-abilities-for-pokemon
  [db pokemon-ids]
  (let [ids (join ", " pokemon-ids)]
    (->>
     (query db
            [(format "select pokemon_abilities.pokemon_id, abilities.ability_id as id, abilities.name 
                         from abilities 
                         inner join pokemon_abilities on pokemon_abilities.ability_id = abilities.ability_id 
                         where pokemon_abilities.pokemon_id in (%s)" ids)])
     (group-by :pokemon_id))))

(defn list-pokemons
  [db]
  (query db
         ["select pokemon_id as id, name, height, weight, is_default from pokemon"]))

(defn ^:private query-for-list-pokemon-with-types
  [type-names]
  (let [where-clause (->> type-names
                          (map #(format "types.name like '%s'" %))
                          (join " or "))]
    (format "select pokemon.pokemon_id as id, pokemon.name, height, weight, is_default
             from pokemon
             where pokemon.pokemon_id in
                 (select pokemon_types.pokemon_id
                  from types
                  inner join pokemon_types on pokemon_types.type_id = types.type_id
                  where %s
                  group by pokemon_types.pokemon_id
                  having count (pokemon_id) = ?)", where-clause)))

(defn list-pokemon-with-types
  [db type-names]
  (if (seq type-names)
    (query db
           [(query-for-list-pokemon-with-types type-names), (count type-names)])
    []))

(defn user-for-token
  [db token]
  (->
   (query db ["select user_id, name from users where token = ?", token])
   first))

(defn add-user!
  [db username hash]
  (with-open [conn (jdbc/connection db)]
    (-> (jdbc/execute conn ["insert into users (name, password) values 
                              (?, ?)
                              on conflict (name)
                              do nothing", username, hash])
        pos?)))

(defn find-user
  [db username]
  (->
   (query db ["select users.user_id, users.name, users.role, users.password
                   from users
                   where users.name = ?", username])
   first))

(defn promote-user-to-admin-role!
  [db username]
  (with-open [conn (jdbc/connection db)]
    (-> (jdbc/execute db ["update users
                          set role = 'admin'
                          where name = ?", username])
        pos?)))
