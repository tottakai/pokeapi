(defproject pokeapi "0.1.0-SNAPSHOT"
  :description "GraphQL PokeApi implementation"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [compojure "1.6.2"]
                 [com.walmartlabs/lacinia "0.37.0"]
                 [ring/ring-core "1.8.2"]
                 [ring/ring-jetty-adapter "1.8.2"]
                 [ring-cors "0.1.13"]
                 [io.aviso/logging "0.3.2"]
                 [org.clojure/tools.logging "1.1.0"]
                 [funcool/clojure.jdbc "0.9.0"]
                 [org.postgresql/postgresql "42.2.18.jre7"]
                 [com.mchange/c3p0 "0.9.5.5"]
                 [environ "1.2.0"]
                 [cheshire "5.10.0"]
                 [clj-http "3.10.3"]
                 [ragtime "0.8.0"]
                 [buddy/buddy-auth "2.2.0" :exclusions [cheshire]]
                 [buddy/buddy-hashers "1.4.0"]
                 [buddy/buddy-sign "3.2.0"]]
  :resource {:resource-path ["resource/graphiql"]}
  :main ^:skip-aot pokeapi.server
  :min-lein-version "2.8.3"
  :plugins [[lein-ring "0.12.5"]]
  :ring {:handler pokeapi.server/app-handler
         :auto-reload? true :auto-refresh? true}
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all
                       :main pokeapi.server}
             :dev {:plugins [[com.jakemccrary/lein-test-refresh "0.24.1"]]}}
  :aliases {"migrate"  ["run" "-m" "pokeapi.migration/migrate"]
            "rollback" ["run" "-m" "pokeapi.migration/rollback"]})
