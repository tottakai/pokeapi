(ns user
  (:require
   [pokeapi.schema :as s]
   [pokeapi.db :as db]
   [com.walmartlabs.lacinia :as lacinia]
   [clojure.java.browse :refer [browse-url]]
   [pokeapi.test-utils :refer [simplify]]
   [ragtime.jdbc :as jdbc]
   [clj-http.client :as client]))

(defn net-query
  [query-string token]
  (client/post "http://localhost:8888/graphql"
               {:body (format "{\"query\":\" %s\"}" query-string)
                :content-type :json
                :headers {"Authorization" (str "Token " token)}}))

(defn q
  [query-string]
  (let [schema (s/load-schema)]
    (-> (lacinia/execute schema query-string nil nil)
        simplify)))
