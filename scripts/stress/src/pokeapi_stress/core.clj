(ns pokeapi-stress.core
  (:gen-class)
  (:require
   [clj-gatling.core :as gatling]
   [org.httpkit.client :as http]))

(def environments {:localhost "http://localhost:8888"
                   :production "https://mypokeapiwebapp.azurewebsites.net"})

(defn- ping [context]
  (let [base-url (:environment context)]
    (= (:status @(http/get (str base-url "/ping"))) 200)))

(defn- graphql-query
  [context body]
  (let [base-url (:environment context)
        {:keys [status]} @(http/post (str base-url "/graphql") {:content-type :json
                                                                :accept :json
                                                                :body body})]
    (= status 200)))

(defn get-pokemon [context]
  (graphql-query context "{\"query\":\"{ pokemon_by_id(id:3) { id name stats {name}}}\"}"))

(defn list-pokemon [context]
  (graphql-query context "{\"query\":\"{ list_pokemon { id name }}\"}"))

(defn list-pokemon-with-stats [context]
  (graphql-query context "{\"query\":\"{ list_pokemon { id name stats { name } }}\"}"))

(def ping-simulation
  {:name "Test ping performance"
   :scenarios [{:name "ping api call"
                :steps [{:name "ping endpoint"
                         :request ping}]}]})

(def retrieve-pokemon-simulation
  {:name "Retrieve individual pokemon simulation"
   :scenarios [{:name "Localhost stress test GET pokemon scenario"
                :steps [{:name "GraphQL endpoing"
                         :request get-pokemon}]}]})

(def list-pokemon-simulation
  {:name "Retrieve list of pokemon simulation"
   :scenarios [{:name "Localhost stress test GET pokemon scenario"
                :steps [{:name "GraphQL endpoing"
                         :request list-pokemon}]}]})

(def list-pokemon-with-stats-simulation
  {:name "Retrieve list of pokemon with stats simulation"
   :scenarios [{:name "Localhost stress test GET pokemon scenario"
                :steps [{:name "GraphQL endpoing"
                         :request list-pokemon-with-stats}]}]})

(def all-out-stress-simulation
  {:name "Stress simulation"
   :scenarios [{:name "Localhost stress test GET pokemon scenario"
                :steps [{:name "GraphQL endpoing"
                         :request get-pokemon}]}
               {:name "Localhost stress test list pokemon scenario"
                :steps [{:name "GraphQL endpoing"
                         :request list-pokemon}]}]})

(def simulations
  {:ping ping-simulation
   :one retrieve-pokemon-simulation
   :list list-pokemon-simulation
   :list-with-stats list-pokemon-with-stats-simulation
   :all-out all-out-stress-simulation})

;lein run list-with-stats production 10
(defn -main [simulation environment request]
  (let [base-url (or ((keyword environment) environments)
                     (throw (Exception. (str "No such environment" environment))))
        simulation (or ((keyword simulation) simulations)
                       (throw (Exception. (str "No such simulation " simulation))))]
    (gatling/run simulation
                 {:context {:environment base-url}
                  :timeout-in-ms 30000
                  :root "/tmp"
                  :concurrency 100
                  :requests (read-string request)
                  :error-file "/tmp/error.log"})))
